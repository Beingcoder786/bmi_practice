package com.example.bmi

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*  //  val drop_down = resources.getStringArray(R.array.drop_down)
        val spinner: Spinner = findViewById(R.id.spinner)
        val height_spinner: Spinner = findViewById(R.id.height_spinner)
        var drop_down= arrayOf("kg","pound","gram")
        if (spinner != null) {
            val adapter = ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, drop_down)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long)
                {
                    /*Toast.makeText(this@MainActivity,
                            getString(R.string.selected_item) + " " +
                                    "" + drop_down[position], Toast.LENGTH_SHORT).show()*/
                    Toast.makeText(getApplicationContext(),drop_down[position] , Toast.LENGTH_LONG).show();
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
        if (height_spinner != null) {
            val adapter = ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, drop_down)
            height_spinner.adapter = adapter
            height_spinner.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    Toast.makeText(this@MainActivity,
                            getString(R.string.selected_item) + " " +
                                    "" + drop_down[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }


        */

        val num_one: Button = findViewById(R.id.num_one)
        val num_two: Button = findViewById(R.id.num_two)
        val num_three: Button = findViewById(R.id.num_three)
        val num_four: Button = findViewById(R.id.num_four)
        val num_five: Button = findViewById(R.id.num_five)
        val num_six: Button = findViewById(R.id.num_six)
        val num_seven: Button = findViewById(R.id.num_seven)
        val num_eight: Button = findViewById(R.id.num_eight)
        val num_nine: Button = findViewById(R.id.num_nine)
        val num_zero: Button = findViewById(R.id.num_zero)
        val num_go: Button = findViewById(R.id.num_go)
        val num_dot: Button = findViewById(R.id.num_dot)
        val num_back: Button = findViewById(R.id.num_back)
        val num_clear: Button = findViewById(R.id.num_clear)

        val weight: TextView = findViewById(R.id.weight)
        val height: TextView = findViewById(R.id.height)

        var w1: String = ""
        var h1: String = ""

        /*    weight.setOnClickListener {
            weight.isSelected = true
            height.isSelected = false
            weight.setTextColor(Color.parseColor("#FF7433"))
        }

        height.setOnClickListener {
            height.isSelected = true
            weight.isSelected = false
            height.setTextColor(Color.parseColor("##C39BD3 "))
        }*/
        weight.setOnClickListener {
            weight.isSelected = true
            height.isSelected = false
            height.setTextColor(Color.parseColor("#A9A9A9"))
            weight.setTextColor(Color.parseColor("#FF7433"))
            if (weight.isSelected) {

                num_one.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "1"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_two.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "2"
                    w1 = buclick1
                    weight.setText(buclick1)

                }
                num_three.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "3"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_four.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "4"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_five.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "5"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_six.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "6"
                    w1 = buclick1
                    weight.setText(buclick1)

                }
                num_seven.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "7"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_eight.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "8"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_nine.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "9"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_zero.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "0"
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_dot.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    buclick1 += "."
                    w1 = buclick1
                    weight.setText(buclick1)
                }
                num_back.setOnClickListener {

                    var str = weight.text.toString()
                    if (str == null || str.isEmpty()) {

                    } else {
                        w1 = ""
                        val new = str.substring(0, str.length - 1)

                        weight.setText(new)
                        w1 = weight.text.toString()


                    }
                }
                num_clear.setOnClickListener {
                    var buclick1 = weight.text.toString()
                    weight.setText("")
                }
            }

        }
        height.setOnClickListener {
            height.isSelected = true
            weight.isSelected = false
            weight.setTextColor(Color.parseColor("#A9A9A9"))
            height.setTextColor(Color.parseColor("#FF7433"))
            if (height.isSelected) {
                num_one.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "1"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_two.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "2"
                    h1 = buclick2
                    height.setText(buclick2)

                }
                num_three.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "3"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_four.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "4"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_five.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "5"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_six.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "6"
                    h1 = buclick2
                    height.setText(buclick2)

                }
                num_seven.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "7"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_eight.setOnClickListener {
                    var buclick2 = weight.text.toString()
                    buclick2 += "8"
                    h1 = buclick2
                    weight.setText(buclick2)
                }
                num_nine.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "9"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_zero.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "0"
                    h1 = buclick2
                    height.setText(buclick2)
                }
                num_dot.setOnClickListener {
                    var buclick2 = height.text.toString()
                    buclick2 += "."
                    h1 = buclick2
                    height.setText(buclick2)

                }
                num_back.setOnClickListener {

                    var str = height.text.toString()
                    if (str == null || str.isEmpty()) {

                    } else {
                        val new = str.substring(0, str.length - 1)
                        h1 = ""
                        height.setText(new)
                        h1 = height.text.toString()


                    }
                }
                num_clear.setOnClickListener {
                    var buclick2 = height.text.toString()
                    height.setText("")
                }
            }

        }


       /*  num_go.setOnClickListener {  //running

             h1=(h1.toDouble()/100.0).toString()
            var result_id:TextView=findViewById(R.id.result_id)
           val n1=h1.toDouble()*h1.toDouble()
           val result=w1.toDouble()/n1.toDouble()
           val re="%.2f".format(result).toDouble()
           Log.d("MainActivity","$result")
           Toast.makeText(this,"${re}",Toast.LENGTH_SHORT).show()
           result_id.setText(re.toString())
       }*/

        /* num_go.setOnClickListener {  //try to show another view and running successfully

           val result_bmi:TextView=findViewById(R.id.bmi)
            h1=(h1.toDouble()/100.0).toString()
            var result_id:TextView=findViewById(R.id.result_id)
            val n1=h1.toDouble()*h1.toDouble()
            val resultt=w1.toDouble()/n1.toDouble()
            val re="%.2f".format(resultt).toDouble()
            Log.d("MainActivity","$resultt")
            result_bmi.setText(re.toString())

           // bmi_result_view.setVisibility(View.VISIBLE)

            val include:View=findViewById(R.id.include)//when no any type then use View
            include.setVisibility(View.GONE)
            val bmi_result_view:View=findViewById(R.id.bmi_result_view)
           bmi_result_view.setVisibility(View.VISIBLE)


        }*/

     num_go.setOnClickListener {//try to different activity


            val result_bmi: TextView = findViewById(R.id.bmi)
            h1 = (h1.toDouble() / 100.0).toString()
            var result_id: TextView = findViewById(R.id.result_id)
            val n1 = h1.toDouble() * h1.toDouble()
            val resultt = w1.toDouble() / n1.toDouble()
            var re = "%.2f".format(resultt).toDouble()

           Log.d("MainActivity", "$resultt")
            //result_bmi.setText(re.toString())
            intent = Intent(this,Result_Bmi::class.java)
            intent.putExtra("yupp",re.toString())
            intent.putExtra("weight",w1.toString())
           intent.putExtra("height",h1.toString())
            startActivity(intent)
            // bmi_result_view.setVisibility(View.VISIBLE)

        }
    }


}






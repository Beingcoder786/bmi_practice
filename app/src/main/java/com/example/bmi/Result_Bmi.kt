package com.example.bmi

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class Result_Bmi : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result__bmi)
        val bmi:TextView=findViewById(R.id.bmi)
        val weightId:TextView=findViewById(R.id.weight)
        val heightId:TextView=findViewById(R.id.height)
        val name = intent.getStringExtra("yupp")

        val weight = intent.getStringExtra("weight")
        val height = intent.getStringExtra("height")
        //birthdayGreeting.text = "Happy Birthday\n$name!"
        bmi.setText(name)
        weightId.setText(weight)
        heightId.setText(height)

        Log.d("message","$name")
        bmi.setTextColor(Color.parseColor("#FF7433"))

        val bmivalue=bmi.text.toString()

        val info:TextView=findViewById(R.id.textView5)

        if(bmivalue.toDouble()>=16.0&&bmivalue.toDouble()<=18.5)
        {
            info.setText("UnderWeight")
        }
            if(bmivalue.toDouble()>18.5&&bmivalue.toDouble()<=25.0)
            {

                info.setText("Normal")
            }

        if(bmivalue.toDouble()>25.0&&bmivalue.toDouble()<=40.5)
        {

            info.setText("OverWeight")
        }
        else if(bmivalue.toDouble()<16.0||bmivalue.toDouble()>40.0)
        {
            info.setText("Obese")
        }
    }
}